<#
    .SYNOPSIS
        Powershell script to update VESDesktop-Corvette.exe.config

    .DESCRIPTION
        This Powershell script will add Max Pool Size = 200 if doesn't exist
    .Example
        
 #>


 param (
    [string]$SimServiceConfigLocation = ""
)


function Update-PMSimServiceConfig {
    param (
        # MaxPoolSize
        [string]$MaxPoolSizeEntry
    )
    

    
    [XML]$SimServiceConfig = Get-Content $SimServiceConfigLocation

    $ConnectionStringLocation = $SimServiceConfig.configuration.connectionStrings.add | Where-Object { $_.Name -EQ 'VES.PMSim.Core'} 
    #value to be set
    $ConnectionStringValue = $ConnectionStringLocation.connectionString 

    Write-host "Connection string value is: $ConnectionStringValue"

    if ($ConnectionStringValue -like "*max pool size=200*" ) {
        
        Write-Host "Connection string contains max pool size=200"

    } Else {
    
        Write-Host "Connection string did not contain Max pool size =200...Appending connection string."
        
        $ConnectionStringLocation.connectionString = $ConnectionStringValue+$MaxPoolSizeEntry
        $SimServiceConfig.Save("$($SimServiceConfigLocation)")

    }


}

$UpdatePMSimServiceConfigArgs = @{

    MaxPoolSizeEntry = "max pool size=200;"

}


Update-PMSimServiceConfig @UpdatePMSimServiceConfigArgs 
