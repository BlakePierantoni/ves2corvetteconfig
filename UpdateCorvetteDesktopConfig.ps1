<#
    .SYNOPSIS
        Powershell script to update VESDesktop-Corvette.exe.config

    .DESCRIPTION
        This Powershell script will add a key to the appSettings Node, add a key to the plugin Paths node, and delete values underneath connection strings node. 
    .Example
        .\UpdateCorvetteDesktopConfig.ps1 -ConfigLocation "C:\Temp\VESDesktop-Corvette.exe.config" -PluginsPathValue "C:\Program Files\PrattMiller\plugins\PmSimPlugin\2.1.17.05131\"
 #>
 

param (
    [string]$ConfigLocation = "",
    [string]$PluginsPathValue = ""
)

 function Update-SIMServiceLocation {
    param (
        # SimService Location Value to add to XML
        [Parameter(Mandatory = $true)]
        [string]$SIMServiceLocationValue
        
    )
    
    [XML]$VESDesktopConfig = Get-Content $ConfigLocation

    $SIMServiceLocation = $VESDesktopConfig.configuration.appSettings.add | Where-Object { $_.Key -EQ 'VES.PMSim.Proxy.Service_Location'} 
    #value to be set
    $SIMServiceLocation.value = $SIMServiceLocationValue

    $VESDesktopConfig.Save("$($ConfigLocation)")

}

function Update-PluginsPath {
   
    [XML]$VESDesktopConfig = Get-Content $ConfigLocation

    $PluginsPathLocation = $VESDesktopConfig.configuration.pluginPaths.add | Where-Object { $_.Key -EQ 'PmSimPlugin'} 
    #value to be set
    $PluginsPathLocation.value = $PluginsPathValue

    $VESDesktopConfig.Save("$($ConfigLocation)")

}

 
$UpdateSimServiceLocationArguments = @{

    SIMServiceLocationValue = "C:\Program Files\PrattMiller\PmSimProxyService\VES.PMSim.Proxy.Service.exe"
}



Update-SIMServiceLocation @UpdateSimServiceLocationArguments
Update-PluginsPath 